Master-Mind
---------------

This is a very popular children's game in Bulgaria, somehow similar and probably an ancestor to the famous Mastermind guessing your oponent's number.

Rules
---------------
    > All digits in the secret number are different.
    > The secret number cannot start with zero. (for the 'official' Bulls and cows)
    > If your try has matching digits on the exact places, they are Bulls.
    > If you have digits from the secret number, but not on the right places, they are Cows.
    > In the lower text area is added your proposition and the number of bulls and cows that match. 