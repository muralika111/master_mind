import random
class MasterMind:
    def __init__(self):
        self.chances_left=0
    
    def set_number_length(self,temp):
        
        if (temp.isnumeric() and temp!="0"):
            self.n=int(temp)
            self.answer =self.set_answer()
            return {"input_digit_set":True}
        else:
            return {"input_digit_set":False}


    def set_answer(self):
        return random.randrange(10**(self.n-1),10**self.n)

    def guess_number(self,guessed_number):
        self.number=guessed_number
        if len(self.number)!=self.n or self.number.isnumeric()==False or self.number[0]=="-":
            
            return {"invalid_input":True}
        else:
            return {"invalid_input":False}
    def initialise_values(self):
        digits=input("enter the no of digits")
        temp=self.set_number_length(digits)
        if temp["input_digit_set"]:
            self.set_answer()
        else:
            print("invalid input")
            self.initialise_values()
    
    def guess(self):              
        self.is_correct=False
        a=list(str(self.answer))
        b=list(self.number)
        self.bulls=self.calculate_bulls(a,b)
        self.cows=self.calculate_cows(a,b)-self.bulls
        self.difference= self.calculate_difference(int(self.answer),int(self.number))
        self.chances_left+=1
        
        if self.difference==0:
            self.is_correct=True
        return {'is_correct': self.is_correct,
                'cows': self.cows,
                'bulls': self.bulls,
                'difference':self.difference,
                }
    def play(self):
        guessed_number=input("Enter the guessed number : ")
        temp_variable=self.guess_number(guessed_number)
        if (temp_variable["invalid_input"]==True):
            print("Invalid input")
            self.play()
               
        self.result=self.guess()
        if self.result['is_correct']:
            print("Bulls eye!!!, you got it right\n\n")
            if (input("wanna play it again?[Y/N]: ")=="Y"):
                    self.reset()
                    self.play()
            else:                    
                print("Thank you")
                return

        elif self.chances_left==10 :
                print("Done with 10 chances\n")
                try_again=input("wanna play it again?[Y/N]: ")
                if try_again=="Y":
                    self.reset()
                    self.play()
                    
                else:
                    self.reset()
                    print("Thank you")
        else:
            print("Attempts : {} " .format(self.chances_left))
            print("cows: {}, bulls: {}, and you are {} ".format(self.cows, self.bulls, self.difference))
        #   print(self.result)
            print("\n--------")
            self.play()



    

    def calculate_bulls(self,a,b):
        self.bulls=0
        for i in range(self.n):
            if a[i]==b[i]:
                self.bulls+=1
        return self.bulls

    def calculate_cows(self,a,b):
        self.cows=0
        temp=[*b]
        for i in a:
            if i in temp:
                self.cows+=1
                temp=temp[:temp.index(i)]+temp[temp.index(i)+1:]
        return self.cows
    def calculate_difference(self,answer,number):
        diff = answer-number
        
        difference_message=""
        if diff==0:
            return 0
        elif diff>0:
            difference_message="Close"
            if diff>10**(self.n-1):
                difference_message="Too low"
            
        else:
            if diff<-10**(self.n-1):
                difference_message="Too High"
            else: 
                difference_message="Close"


        return difference_message

    def reset(self):
        self.answer =self.set_answer()
        self.chances_left=0
        


if __name__ == "__main__":
    player=MasterMind()
    player.initialise_values()    
    player.play()



        




        
            
    
        


    

