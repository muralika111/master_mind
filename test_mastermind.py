import sys
from master_mind import MasterMind

def test_mastermind():
    test_length = '2'
    m=MasterMind()
    
    #test case 1--- testing  function 
    
    test_result = m.set_number_length(test_length)
    result = {"input_digit_set":True}
    
    assert test_result == result
    
    #test case 2-- set_number_length function
    
    test_length = 'abc'
    result = {"input_digit_set":False}
    test_result = m.set_number_length(test_length)
    
    assert result == test_result
    
    #test case 2.1-- test guess_number function
    
    test_length = '0'
    result = {"input_digit_set":False}
    test_result = m.set_number_length(test_length)
    
    assert result == test_result

    #test case --3 test input_guess function(checking weather the input is number or not)
    m.n = 5
    test_guess = '12345'
    result = {"invalid_input":False}
    test_result = m.guess_number(test_guess)
    assert result == test_result
    
    #test case --3 test input_guess function(checking weather the input is number or not)
    m.n = 5
    test_guess = 'tdg45'
    result = {"invalid_input":True}
    test_result = m.guess_number(test_guess)
    assert result == test_result
    
    #test case - 4 test the length of the inputted string
    m.n = 5
    test_guess = 'ygd' 
    result = {"invalid_input":True}
    test_result =  m.guess_number(test_guess)
    assert result == test_result

    #test case -5 test the working of guess function
    m.n = 5
    m.answer = '34567'
    m.number = '34567'
    result = True
    m.guess()
    test_result = m.is_correct
    assert result == test_result
    
    #test case -6 test the working of the guess function at the extremes
    m.n = 5
    m.answer = '34567'
    m.number = '00000'
    result = {'is_correct': False,
                'cows': 0,
                'bulls': 0,
                'difference':"Too low",
                }
    test_result = m.guess()
    assert result == test_result
    
    
    #test case -7.2 test the working of the guess function at the extremes
    m.n = 5
    m.answer = '34567'
    m.number = '99999'
    result = {'is_correct': False,
                'cows': 0,
                'bulls': 0,
                'difference':"Too High",
                }
    test_result = m.guess()
    assert result == test_result
    
    #test case -7.2 test the working of the guess function at the extremes
    m.n= 5
    m.answer = '99999'
    m.number = '99999'
    result = {'is_correct':True,
              'cows': 0,
              'bulls': 5,
              'difference':0,
                }
    test_result = m.guess()
    assert result == test_result
    
    #test case - 8 test the working of guess function at intermediate levels
    m.n = 5
    m.answer = '34783'
    m.number = '56789'
    result = {'is_correct': False,
              'cows': 0,
              'bulls': 2,
              'difference':"Too High",
                }
    test_result = m.guess()
    assert result == test_result
    
    print('\n Congratulations, all test cases successful')

test_mastermind()
